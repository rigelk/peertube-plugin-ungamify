function register ({ registerHook, peertubeHelpers }) {
  initUngamify(registerHook, peertubeHelpers)
    .catch(err => console.error('Cannot initialize ungamify plugin', err))
}

export {
  register
}

function initUngamify (registerHook, peertubeHelpers) {
  return peertubeHelpers.getSettings()
    .then(s => {
      const clean = () => {
        if (s["ungamify-views-counter"] === "true") {
          [
            "views"
          ].map(className =>
            [...document.getElementsByClassName(className)].forEach(node => node.style.visibility = "hidden")
          )
          [
            "my-video-views-counter"
          ].map(tagName =>
            [...document.getElementsByTagName(tagName)].forEach(node => node.style.visibility = "hidden")
          )
        }
      }

      var mutationObserver = new MutationObserver(function(mutations) {
        clean()
      })
      mutationObserver.observe(document.documentElement, {
        attributes: true,
        characterData: true,
        childList: true,
        subtree: true,
        attributeOldValue: true,
        characterDataOldValue: true
      })

      setTimeout(clean, 50)

      registerHook({
        target: "action:router.navigation-end",
        handler: params => clean()
      })
    })
}
