function register ({ registerHook, peertubeHelpers }) {
  initUngamify(registerHook, peertubeHelpers)
    .catch(err => console.error('Cannot initialize ungamify plugin', err))
}

export {
  register
}

function initUngamify (registerHook, peertubeHelpers) {
  return peertubeHelpers.getSettings()
    .then(s => {
      registerHook({
        target: "action:video-watch.player.loaded",
        handler: _ => {
          if (s["ungamify-likes"] === "true") {
            [
              "action-button-like",
              "action-button-dislike",
              "video-info-likes-dislikes-bar"
            ].map(className =>
              [...document.getElementsByClassName(className)].forEach(node => node.style.visibility = "hidden")
            )
          }
          if (s["ungamify-views-counter"] === "true") {
            [
              "views"
            ].map(className =>
              [...document.getElementsByClassName(className)].forEach(node => node.style.visibility = "hidden")
            )
            [
              "my-video-views-counter"
            ].map(tagName =>
              [...document.getElementsByTagName(tagName)].forEach(node => node.style.visibility = "hidden")
            )
          }
          if (s["ungamify-replies-counter"] === "true") {
            [
              "view-replies"
            ].map(className =>
              [...document.getElementsByClassName(className)].forEach(node => {
                const innerElements = node.innerHTML.split("<!---->")
                innerElements[1] = innerElements[1].replace(/[0-9]/g, '').trim()
                node.innerHTML = innerElements.join("<!---->")
              })
            )
          }
        }
      })
    })
}
