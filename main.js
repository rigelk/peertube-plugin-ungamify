async function register ({
  registerSetting,
}) {
  registerSetting({
    name: "ungamify-likes",
    label: "Hide likes and like buttons",
    type: "input",
    private: false,
    default: "true"
  })
  registerSetting({
    name: "ungamify-views-counter",
    label: "Hide views counter",
    type: "input",
    private: false,
    default: "true"
  })
  registerSetting({
    name: "ungamify-replies-counter",
    label: "Hide replies counter",
    type: "input",
    private: false,
    default: "true"
  })
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
