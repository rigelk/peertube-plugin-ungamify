# Ungamify

PeerTube uses slight gamification (i.e.: like buttons), and even if it is a far cry from Facebook's or Google's,
less or no gamification at all is a choice users like you might want to make. This plugin is for you.

## Read First

This plugin is NOT supported by Framasoft, nor any organisation.
