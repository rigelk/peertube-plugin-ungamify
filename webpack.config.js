const path = require("path")

const EsmWebpackPlugin = require("@purtuga/esm-webpack-plugin")

let commonConfig = {
  entry: "./client/common-client-plugin.js",
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "./common-client-plugin.js",
    library: "script",
    libraryTarget: "var"
  },
  plugins: [new EsmWebpackPlugin()]
}

let videoWatchConfig = {
  entry: "./client/video-watch-client-plugin.js",
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "./video-watch-client-plugin.js",
    library: "script",
    libraryTarget: "var"
  },
  plugins: [new EsmWebpackPlugin()]
}

module.exports = [
  commonConfig,
  videoWatchConfig
]
